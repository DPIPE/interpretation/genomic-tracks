FROM ubuntu:focal-20220426

RUN apt-get update && \
    apt-get install -y \
    gawk \
    git \
    git-lfs \
    python3-pip \
    bedtools \
    rsync \
    wget \
    autoconf \
    automake \
    libcurl4 \
    vcftools \
    bcftools

WORKDIR /work

ENV LANGUAGE=C.UTF-8 \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8

RUN rsync -av \
   rsync://hgdownload.soe.ucsc.edu/genome/admin/exe/linux.x86_64/bedGraphToBigWig \
   rsync://hgdownload.soe.ucsc.edu/genome/admin/exe/linux.x86_64/bedToGenePred \
   rsync://hgdownload.soe.ucsc.edu/genome/admin/exe/linux.x86_64/bigBedToBed \
   rsync://hgdownload.soe.ucsc.edu/genome/admin/exe/linux.x86_64/genePredToGtf  \
   rsync://hgdownload.soe.ucsc.edu/genome/admin/exe/linux.x86_64/genePredSingleCover /work/bin/

ENV PERL5LIB=/work/src/perl
RUN mkdir /tracks /data

ENV PATH="/work/bin:${PATH}" \
    INPUT_DIR=/work/input

COPY . /work