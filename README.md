# Protocol for generating IGV/igv.js tracks for CNV annotations

## List of generated tracks
- RefSeq Curated: `ncbiRefSeqCurated.sorted.gtf.gz`
- Gene-based annotations: `ncbiRefSeqCurated.cover.annotated.vcf.gz`
- ClinGen Prenatal (nstd75):
    - `clingen.nstd75.GRCh37.variant_call.class1-2.vcf.gz`
    - `clingen.nstd75.GRCh37.variant_call.class3.vcf.gz`
    - `nstd75.GRCh37.variant_call.class4-5.vcf.gz`
- ClinVar Clin SV (nstd102):
    - `clingen.nstd102.GRCh37.variant_call.class1-2.vcf.gz`
    - `clingen.nstd102.GRCh37.variant_call.class3.vcf.gz`
    - `clingen.nstd102.GRCh37.variant_call.class4-5.vcf.gz`
- ClinGen Curated regions:
    - `ClinGen_region_curation_list_GRCh37.bed`
- gnomAD:
    - all: `gnomad_v2.1_sv.sites.bed.gz`
    - controls only: `gnomad_v2.1_sv.controls_only.sites.bed.gz`
    - non-neuro: `gnomad_v2.1_sv.nonneuro.sites.bed.gz`
- DGV via UCSC: `dgvGold.bb`
- uniprot domains via UCSC: `unipDomain.bb`
____

Most of the tracks are downloaded from the respective sources, except for:
 - OMIM data that is supplied from gene-panel builder (to fetch directly once it is checked in)
 - OUSAMG data that we pass internally and process on TSD: 
    -   `ousamg.class1_CG_aug2020_hg19.bed`
    -   `ousamg.class4-5_hg19.pooled.bed`


For igv.js all tracks needed to be 
- sorted
- archived with bgzip
- indexed with tabix

## Current use of the pipeline: 

```
# run the wrapper script to generate all tracks
make make-tracks
```
The created tracks will be in the sub-folder `output`. 

Sub-commands for use in troubleshooting or further development:
```
# Build Docker
make build

# run the shell to get inside the container
make shell

```


