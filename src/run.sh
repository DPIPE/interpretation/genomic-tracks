#!/usr/bin/env bash

# invoke all other scripts 

# clingen studies
echo "Create Clingen studies tracks"
bash src/clingen_nstd.sh

# clingen curated
echo "Create Clingen Curate track"
bash src/clingen_regions.sh

# gnomad
echo "create gnomAD SV tracks"
bash src/gnomad_variants.sh

# gene-based and genes
echo "Create gene-based tracks"
bash src/gene_based.sh

echo "Skip uniprot and DGV tracks until supported by ELLA"
# uniprot domains
# bash src/uniprot.sh

# DGV
# bash src/dgv.sh 