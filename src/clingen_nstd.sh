#!/usr/bin/env bash

# ClinGen Prenatal (nstd75) and Postnatal (nstd102)
mkdir -p data/ClinGen/dbvar
cd data/ClinGen/dbvar
wget https://ftp.ncbi.nlm.nih.gov/pub/dbVar/data/Homo_sapiens/by_study/vcf/nstd75.GRCh37.variant_call.vcf.gz
wget https://ftp.ncbi.nlm.nih.gov/pub/dbVar/data/Homo_sapiens/by_study/vcf/nstd75.GRCh37.variant_call.vcf.gz.tbi

bcftools filter -i 'INFO/CLNSIG~"athogenic"' nstd75.GRCh37.variant_call.vcf.gz | bgzip -c > clingen.nstd75.GRCh37.variant_call.class4-5.vcf.gz
tabix nstd75.GRCh37.variant_call.class4-5.vcf.gz

bcftools filter -i 'INFO/CLNSIG~"Uncertain"' nstd75.GRCh37.variant_call.vcf.gz | bgzip -c > clingen.nstd75.GRCh37.variant_call.class3.vcf.gz
tabix clingen.nstd75.GRCh37.variant_call.class3.vcf.gz

bcftools filter -i 'INFO/CLNSIG~"enign"' nstd75.GRCh37.variant_call.vcf.gz | bgzip -c > clingen.nstd75.GRCh37.variant_call.class1-2.vcf.gz
tabix clingen.nstd75.GRCh37.variant_call.class1-2.vcf.gz

wget https://ftp.ncbi.nlm.nih.gov/pub/dbVar/data/Homo_sapiens/by_study/vcf/nstd102.GRCh37.variant_call.vcf.gz
wget https://ftp.ncbi.nlm.nih.gov/pub/dbVar/data/Homo_sapiens/by_study/vcf/nstd102.GRCh37.variant_call.vcf.gz.tbi

bcftools filter -i 'INFO/CLNSIG~"athogenic"' nstd102.GRCh37.variant_call.vcf.gz | bgzip -c > clingen.nstd102.GRCh37.variant_call.class4-5.vcf.gz
tabix clingen.nstd102.GRCh37.variant_call.class4-5.vcf.gz

bcftools filter -i 'INFO/CLNSIG~"Uncertain"' nstd102.GRCh37.variant_call.vcf.gz | bgzip -c > clingen.nstd102.GRCh37.variant_call.class3.vcf.gz
tabix clingen.nstd102.GRCh37.variant_call.class3.vcf.gz

bcftools filter -i 'INFO/CLNSIG~"enign"' nstd102.GRCh37.variant_call.vcf.gz | bgzip -c > clingen.nstd102.GRCh37.variant_call.class1-2.vcf.gz
tabix clingen.nstd102.GRCh37.variant_call.class1-2.vcf.gz

mv clingen.*vcf.gz* ${MOUNT_DIR}
cd ../../..
