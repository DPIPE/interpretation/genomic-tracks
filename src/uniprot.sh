#!/usr/bin/env bash

## UniProt domains from UCSC
mkdir -p data/uniprot
cd data/uniprot
wget https://hgdownload.soe.ucsc.edu/gbdb/hg19/uniprot/unipDomain.bb

mv unipDomain.bb ${MOUNT_DIR}
cd ../..
