#!/usr/bin/env bash 

VERSION=20220510
mkdir -p data/ClinGen
cd data/ClinGen
wget https://ftp.clinicalgenome.org/archive/${VERSION}/ClinGen_region_curation_list_GRCh37.tsv

# formats expected:

# head -7  ClinGen_region_curation_list_GRCh37.tsv 
# #ClinGen Region Curation Results
# #10 May,2022
# #Genomic Locations are reported on GRCh37 (hg19): GCF_000001405.13
# #https://www.ncbi.nlm.nih.gov/projects/dbvar/clingen
# #to create link: https://www.ncbi.nlm.nih.gov/projects/dbvar/clingen/clingen_region.cgi?id=key
# #ISCA ID        ISCA Region Name        cytoBand        Genomic Location        Haploinsufficiency Score        Haploinsufficiency Description  Haploinsufficiency PMID1   Haploinsufficiency PMID2        Haploinsufficiency PMID3        Haploinsufficiency PMID4        Haploinsufficiency PMID5        Haploinsufficiency PMID6   Triplosensitivity Score Triplosensitivity Description   Triplosensitivity PMID1 Triplosensitivity PMID2 Triplosensitivity PMID3 Triplosensitivity PMID4    Triplosensitivity PMID5 Triplosensitivity PMID6 Date Last Evaluated     Haploinsufficiency Disease ID   Triplosensitivity Disease ID
# ISCA-46742      7p22.1 region (includes ACTB)   7p22.1  chr7:5536848-5799722    3       Sufficient evidence for dosage pathogenicity    27633570        29220674   32562408                                1       Little evidence for dosage pathogenicity        26297194        21998864        22495914        25893121   25124455        27866048        2022-02-08      MONDO:0000508   MONDO:0100038#

## format regions as bed 
gawk -v FS='\t' -v OFS='\t' '!($1~"^#"){
    split($4,a,":"); 
    split(a[2],b,"-"); 
    print a[1], b[1], b[2], $1";"$3";TS="$13, $5
}' ClinGen_region_curation_list_GRCh37.tsv \
  | sort -k1,1V -k2,2n > ${MOUNT_DIR}/ClinGen_region_curation_list_GRCh37.bed

cd ../..
