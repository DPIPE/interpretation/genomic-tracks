#!/usr/bin/env bash 
set -euo pipefail
# This script collects and parses the data from various sources
# The data is then added to a signle file which is formatted as vcf

# Download NCBI RefSeq data, format it as gtf and extract a list of unique gene symbols
bash src/genes.sh
# Download ClinGen files, shape as bed and extract values for vcf: HI, TS
bash src/gene_based/clingen_dosage.sh
# Given input omim summary table (provided via genepanel builder and mounted), extract values:
# HGNC, pheMIM, phenotype, inheritance, Morbid
bash src/gene_based/omim.sh
# Download GenCC table and extract values: GenCC link
bash src/gene_based/genecc.sh
# Download gnomAD data, extract values: pLI, LOEUF
bash src/gene_based/gnomad_metrics.sh
# Download DECIPHER data, extract values: perc_HI, DDG2P
bash src/gene_based/decipher.sh
# Given an in-house file with imprinted/mosaikk gene list, add those values
bash src/gene_based/ousamg_medgenes.sh

cd data
# prepare a backbone vcf file with gene coordinates
# genes/ncbiRefSeqCurated.cover.bed is sorted
cat /work/ops/header_template.hdr > genes/ncbiRefSeqCurated.cover.vcf
gawk -v OFS="\t" '{
    gsub("chr","",$1); 
    print $1, $2, $4, "N", "<GENE>", 0, "PASS", "END="$3, "CN", "."
    }' genes/ncbiRefSeqCurated.cover.bed >> genes/ncbiRefSeqCurated.cover.vcf


# Repeated join for all collected data sources
# explicitely sort the files before using "join" utility
sort -k 1b,1 genes/ncbiRefSeqCurated.cover.symbols.txt > join.tmp

# expected format of join.tmp;
# tab-separated
# GENE SYMBOL FIELD     ANNOTATION1=VALUE1   ANNOTATION2=VALUE2

for file in $(ls */collect_*); do
    sort -k 1b,1 $file > file.txt
    join -a1 -a2 -e 1 -o auto -t $'\t' join.tmp file.txt >join.tmp.1
    mv join.tmp.1 join.tmp
done
# The "$co" variable counts occurences of empty annotations (one of "NA", "no" or "")
# If our of 12 expected annotations there is at least one field annotated - print line for that gene
# otherwise do not print the line (skip genes with no annotations)
gawk -v OFS='\t' '{
    out="";
    co=0; 
    for (i=2;i<=NF;i++) {
        split($i,s,"="); 
        p=s[2]; 
        if (p!="NA" && p!="" && p!="no" || s[1]=="Morbid") {
            gsub("_"," ",p);
            out=out";"s[1]"="p
        }
        else co++
    }; 
    gsub("^;","",out); 
    if (co<12) print $1, out
}' join.tmp > collect.tmp

# "${a[@]}" is an awk array that holds lines of the collected annotations from "collect.tmp" backbone vcf (genes/ncbiRefSeqCurated.cover.vcf) 
# in the form a[GENE_SYMBOL]=ANNOTATION
# when merging it to the backbone vcf (genes/ncbiRefSeqCurated.cover.vcf)
# we want to only print the lines that have values in array "${a[@]}" (i.e. annotated lines) and skip otherwise: if (a[$3]!="") print ...
gawk -v FS='\t' -v OFS='\t' 'NR==FNR{
    a[$1]=$2;
    next
} $1~"^#"{
    print $0;
    next
}{
    if (a[$3]!="") {
        print $1,$2,$3,$4,$5,$6,$7,$8";"a[$3],$9,$10
    }
}' collect.tmp genes/ncbiRefSeqCurated.cover.vcf > genes/ncbiRefSeqCurated.cover.annotated.vcf

# vcf is based on file "genes/ncbiRefSeqCurated.cover.bed" which is sorted by chr, coordinate 
bgzip -c genes/ncbiRefSeqCurated.cover.annotated.vcf > ${MOUNT_DIR}/ncbiRefSeqCurated.cover.annotated.vcf.gz
tabix ${MOUNT_DIR}/ncbiRefSeqCurated.cover.annotated.vcf.gz

cd ..