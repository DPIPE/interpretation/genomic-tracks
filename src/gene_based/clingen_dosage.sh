#!/usr/bin/env bash 

VERSION=20220510
mkdir -p data/ClinGen
cd data/ClinGen
wget https://ftp.clinicalgenome.org/archive/${VERSION}/ClinGen_haploinsufficiency_gene_GRCh37.bed
wget https://ftp.clinicalgenome.org/archive/${VERSION}/ClinGen_triplosensitivity_gene_GRCh37.bed

# formats expected:

# head -3  ClinGen_*_gene_GRCh37.bed 
# ==> ClinGen_haploinsufficiency_gene_GRCh37.bed <==
# track name='ClinGen Gene Curation Haploinsufficiency Scores' db=hg19
# chr1    955502  991499  AGRN    30
# chr1    1167628 1170421 B3GALT6 30

# ==> ClinGen_triplosensitivity_gene_GRCh37.bed <==
# track name='ClinGen Gene Curation Triplosensitivity Scores' db=hg19
# chr1    955502  991499  AGRN    0
# chr1    1167628 1170421 B3GALT6 0

cd ..
## prepare data to add to backbone gene vcf file
gawk -v OFS='\t' 'NR==FNR{
    a[$4]=$5;
    next
}{
    s=a[$1]==""?"NA":a[$1]; 
    print $0,"TS="s 
}' ClinGen/ClinGen_triplosensitivity_gene_GRCh37.bed <(
    gawk 'NR==FNR{
        a[$4]=$5;
        next
    }{
        s=a[$1]==""?"NA":a[$1];
        print $1,"HI="s 
    }' ClinGen/ClinGen_haploinsufficiency_gene_GRCh37.bed  genes/ncbiRefSeqCurated.cover.symbols.txt
) > ClinGen/collect_genes_clingen_dosage.txt
