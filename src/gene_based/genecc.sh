#!/usr/bin/env bash 
mkdir -p data/genecc
cd data/genecc
# Download from gene-cc website as gencc-submissions.txt
wget https://search.thegencc.org/download/action/submissions-export-csv
mv submissions-export-csv gencc-submissions.tsv

# Expected format:
# head -2  data/genecc/gencc-submissions.tsv
# "uuid","gene_curie","gene_symbol","disease_curie","disease_title","disease_original_curie","disease_original_title","classification_curie","classification_title","moi_curie","moi_title","submitter_curie","submitter_title","submitted_as_hgnc_id","submitted_as_hgnc_symbol","submitted_as_disease_id","submitted_as_disease_name","submitted_as_moi_id","submitted_as_moi_name","submitted_as_submitter_id","submitted_as_submitter_name","submitted_as_classification_id","submitted_as_classification_name","submitted_as_date","submitted_as_public_report_url","submitted_as_notes","submitted_as_pmids","submitted_as_assertion_criteria_url","submitted_as_submission_id","submitted_run_date"
# "GENCC_000101-HGNC_10896-OMIM_182212-HP_0000006-GENCC_100001","HGNC:10896","SKI","MONDO:0008426","Shprintzen-Goldberg syndrome","OMIM:182212","Shprintzen-Goldberg syndrome","GENCC:100001","Definitive","HP:0000006","Autosomal dominant","GENCC:000101","Ambry Genetics","HGNC:10896","SKI","OMIM:182212","Shprintzen-Goldberg syndrome","HP:0000006","Autosomal dominant inheritance","GENCC:000101","Ambry Genetics","GENCC:100001","Definitive","2018-03-30 13:31:56","","","","PMID: 28106320","1034","2020-12-24"

# format the data in the unform manner:
# GENE SYMBOL   ANNOTATION1=VALUE1;ANNOTATION2=VALUE2;..
gawk -v FS=',' -v OFS='\t' 'NR>1{
    gsub("\"","",$0);
    print $3, "GenCC=https://search.thegencc.org/genes/"$2
}' gencc-submissions.tsv > genecc_genes.txt

cd ..

# Merge with the backbobe list of genes
gawk -v OFS='\t' 'NR==FNR{
    a[$1]=$2;
    next
} {
    s=a[$1]==""?"GeneCC=NA":a[$1];
    print $0,s
}' genecc/genecc_genes.txt  genes/ncbiRefSeqCurated.cover.symbols.txt > genecc/collect_genes_genecc.txt

cd ..
