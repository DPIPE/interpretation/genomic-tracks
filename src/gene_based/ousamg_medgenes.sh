#!/usr/bin/env bash 
# Format imprinted and mosaic gene lists


mkdir data/medgen_genes
cd data

# Expected format:

# head test/imprinted_mosaic.list
# ENG     mosaic
# HRAS    mosaic
# FLNB    mosaic
# KDM5C   mosaic
# DICER1  mosaic
# CASK    mosaic
# MAGEL2  imprinted

gawk -v OFS="\t" 'NR==FNR{
    a[$1]=$2;
    next
}{
    print $0,"ouslist="a[$1] 
}' ${INPUT_DIR}/imprinted_mosaic.list  genes/ncbiRefSeqCurated.cover.symbols.txt > medgen_genes/collect_genes_ous.txt


### medgen lists
### manually curate HG19
#awk 'NR==FNR{a[$5]=$1":"$2":"$3":"$4;next}{split($1,b,";"); print a[b[1]], $0}' idmap/genes.RefSeq.sorted_tags.bed  medgen_genes/imprinted_mosaic.txt > medgen_genes/imprinted_mosaic.mapped.txt
#awk -v OFS="\t" '{split($1,a,":"); print "chr"a[1],a[2],a[3], $2";"$3, 0, a[4]}'  medgen_genes/imprinted_mosaic.mapped.txt | sort -k1,1V -k2,2n > medgen_genes/imprinted_mosaic.mapped.bed
#awk '$4~"mosaic"'  medgen_genes/imprinted_mosaic.mapped.bed >  medgen_genes/mosaic.mapped.bed
#awk '$4~"imprinted"'  medgen_genes/imprinted_mosaic.mapped.bed > medgen_genes/imprinted.mapped.bed

