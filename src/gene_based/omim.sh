#!/usr/bin/env bash 

mkdir -p data/omim
cd data

# Expected input file formats:

# head -4 ../test/Eksom_v2_phenotypes.tsv
# # Gene panel: Eksom-v2 -- Date: 2021-09-22
# #gene symbol    HGNC    remove (add x)  phenotype       inheritance     omim_number     pmid    inheritance info        comment
# A2ML1   23336           {Otitis media, susceptibility to}       AD      166760
# A4GALT  18149           NOR polyagglutination syndrome          111400

# zcat ../test/20211012_morbid.tsv.gz | head -3
# genes   OMIM_morbid
# 16E1BP  yes
# 2ADUB   yes

# format the input omim data in the unform manner:
# GENE SYMBOL   ANNOTATION1=VALUE1;ANNOTATION2=VALUE2;..
gawk -v FS='\t' -v OFS='\t' '
    NR==1{next}
    NR==2{
        for (i=1; i<=NF; i++) {
            gsub("#","",$i);
            f[$i] = i
        }
    } {
        pheno=$(f["phenotype"]);
        gsub(" ", "_", pheno);
        print($(f["gene symbol"]),
            "HGNC="$(f["HGNC"]),
            "pheMIM="$(f["omim_number"]),
            "phenotype="pheno,
            "inheritance="$(f["inheritance"]))
    }
' ${INPUT_DIR}/Eksom_v2_phenotypes.tsv > omim/omim.table

# Add a Morbid gene table fetched from AnnotSV package
gawk 'NR==FNR{
    a[$1]=$0;
    next
}{
    s=a[$1]==""?$1" HGNC= pheMIM=   phenotype=    inheritance=  Morbid=no":a[$1]; 
    print s
}' <(
    gawk 'NR==FNR{
        a[$1]=$2;
        next
    } {
        m=a[$1]=="yes"?a[$1]:"no";
        print $0, "Morbid="m
    }' \
    <(zcat ${INPUT_DIR}/20211012_morbid.tsv.gz) \
    omim/omim.table
) genes/ncbiRefSeqCurated.cover.symbols.txt  > omim/collect_genes_omim_morbid.txt
