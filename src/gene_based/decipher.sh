#!/usr/bin/env bash

mkdir -p data/decipher
cd data/decipher

# Expected formats:

# zcat HI_Predictions_Version3.bed.gz | head -3
# track name='Haploinsufficiency Predictions' description='Haploinsufficiency Predictions Version 3 (Ni et al). https://decipher.sanger.ac.uk' visibility=4 itemRgb='on'
# chrX    37850070        37850569        HYPM|0.000043663|100%   0.000043663     .       37850070        37850569        0,255,0
# chr5    43039335        43043272        ANXA2R|0.000047349|100% 0.000047349     .       43039335        43043272        0,255,0

# zcat DDG2P.csv.gz | head -2
# "gene symbol","gene mim","disease name","disease mim","confidence category","allelic requirement","mutation consequence",phenotypes,"organ specificity list",pmids,panel,"prev symbols","hgnc id","gene disease pair entry date","cross cutting modifier","mutation consequence flag","confidence value flag",comments,"variant consequence","disease ontology"
# HMX1,142992,"OCULOAURICULAR SYNDROME",612109,strong,biallelic_autosomal,"absent gene product",HP:0000556;HP:0004328;HP:0000007;HP:0000482;HP:0000647;HP:0007906;HP:0000568;HP:0000589;HP:0000519;HP:0000639;HP:0000518;HP:0000505;HP:0001104;HP:0008572,Eye;Ear,25574057;29140751;18423520,DD,,5017,"2015-07-22 16:14:07",,,,,,

wget -O /dev/stdout https://www.deciphergenomics.org/files/downloads/HI_Predictions_Version3.bed.gz |\
zcat |\
gawk -v OFS='\t' 'NR>1{
    split($4,a,"|"); 
    gsub("%","",a[3]); 
    print a[1], "perc_HI="a[3]
}' > decipher_hi.txt

wget -O /dev/stdout https://www.ebi.ac.uk/gene2phenotype/downloads/DDG2P.csv.gz |\
zcat |\
gawk -F',' -v OFS='\t' 'NR>1{
        print $1, "DDG2P=yes"
    }' > ddg2p_genes.txt

cd ..
# format the data in the unform manner:
# GENE SYMBOL   ANNOTATION1=VALUE1;ANNOTATION2=VALUE2;..
gawk -v OFS='\t' 'NR==FNR{
    a[$1]=$2;
    next
}{
    s=a[$1]==""?"perc_HI=":a[$1]; 
    print $1,s 
}' decipher/decipher_hi.txt  genes/ncbiRefSeqCurated.cover.symbols.txt > decipher/collect_genes_decipher.txt

gawk -v OFS='\t' 'NR==FNR{
    a[$1]=$2;
    next
}{
    s=a[$1]==""?"DDG2P=no":a[$1]; 
    print $1,s 
}' decipher/ddg2p_genes.txt  genes/ncbiRefSeqCurated.cover.symbols.txt > decipher/collect_genes_ddg2p.txt

cd ..
