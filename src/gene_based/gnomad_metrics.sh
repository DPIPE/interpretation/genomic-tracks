#!/usr/bin/env bash 

mkdir data/gnomad
cd data/gnomad

wget https://gnomad-public-us-east-1.s3.amazonaws.com/release/2.1.1/constraint/gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz

# Expected format:
# zcat  data/gnomad/gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz | head -2 
# gene    transcript      obs_mis exp_mis oe_mis  mu_mis  possible_mis    obs_mis_pphen   exp_mis_pphen   oe_mis_pphen    possible_mis_pphen      obs_syn exp_syn    oe_syn  mu_syn  possible_syn    obs_lof mu_lof  possible_lof    exp_lof pLI     pNull   pRec    oe_lof  oe_syn_lower    oe_syn_upper    oe_mis_lower       oe_mis_upper    oe_lof_lower    oe_lof_upper    constraint_flag syn_z   mis_z   lof_z   oe_lof_upper_rank       oe_lof_upper_bin        oe_lof_upper_bin_6 n_sites classic_caf     max_af  no_lofs obs_het_lof     obs_hom_lof     defined p       exp_hom_lof     classic_caf_afr classic_caf_amr classic_caf_asj    classic_caf_eas classic_caf_fin classic_caf_nfe classic_caf_oth classic_caf_sas p_afr   p_amr   p_asj   p_eas   p_fin   p_nfe   p_oth   p_sas   transcript_type    gene_id transcript_level        cds_length      num_coding_exons        gene_type       gene_length     exac_pLI        exac_obs_lof    exac_exp_lof       exac_oe_lof     brain_expression        chromosome      start_position  end_position
# MED13   ENST00000397786 871     1.1178e+03      7.7921e-01      5.5598e-05      14195   314     5.2975e+02      5.9273e-01      6708    422     3.8753e+021.0890e+00       1.9097e-05      4248    0       4.9203e-06      1257    9.8429e+01      1.0000e+00      8.9436e-40      1.8383e-16      0.0000e+00      1.0050e+00 1.1800e+00      7.3600e-01      8.2400e-01      0.0000e+00      3.0000e-02              -1.3765e+00     2.6232e+00      9.1935e+00      0       0 02       1.2058e-05      8.0492e-06      124782  3       0       124785  1.2021e-05      1.8031e-05      0.0000e+00      0.0000e+00      0.0000e+00      0.0000e+00 9.2812e-05      8.8571e-06      0.0000e+00      0.0000e+00      0.0000e+00      0.0000e+00      0.0000e+00      0.0000e+00      9.2760e-05      8.8276e-06 0.0000e+00      0.0000e+00      protein_coding  ENSG00000108510 2       6522    30      protein_coding  122678  1.0000e+00      0       6.4393e+010.0000e+00       NA      17      60019966        60142643

# numbering of columns in the header to exctract needed values
# zcat <  gnomAD/gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz  | head -1 | awk '{for (i=1;i<=NF;i++){print i, $i}}'
zcat < gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz \
    | cut -f1,21,30 \
    | gawk -v OFS="\t" 'NR>1{
        printf("%s\tpLI=%.4G\tLOEUF=%.4g\n",$1,$2,$3)
    }' > gnomad_genes.txt

cd ..

gawk -v OFS="\t" 'NR==FNR{
    a[$1]=$2"\t"$3;
    next
}{
    s=a[$1]==""?"pLI=   LOEUF=":a[$1]; 
    print $0,s 
}' gnomad/gnomad_genes.txt  genes/ncbiRefSeqCurated.cover.symbols.txt > gnomad/collect_genes_gnomad.txt

cd ..
