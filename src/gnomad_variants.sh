#!/usr/bin/env bash 

mkdir data/gnomad
cd data/gnomad

# gnomAD controls database
# either direct download or via AnnotSV (identical)
wget https://gnomad-public-us-east-1.s3.amazonaws.com/papers/2019-sv/gnomad_v2.1_sv.sites.bed.gz
wget https://gnomad-public-us-east-1.s3.amazonaws.com/papers/2019-sv/gnomad_v2.1_sv.sites.bed.gz.tbi

wget https://gnomad-public-us-east-1.s3.amazonaws.com/papers/2019-sv/gnomad_v2.1_sv.controls_only.sites.bed.gz
wget https://gnomad-public-us-east-1.s3.amazonaws.com/papers/2019-sv/gnomad_v2.1_sv.controls_only.sites.bed.gz.tbi

wget https://gnomad-public-us-east-1.s3.amazonaws.com/papers/2019-sv/gnomad_v2.1_sv.nonneuro.sites.bed.gz
wget https://gnomad-public-us-east-1.s3.amazonaws.com/papers/2019-sv/gnomad_v2.1_sv.nonneuro.sites.bed.gz.tbi

cp gnomad*.bed.gz* ${MOUNT_DIR}

cd ../..