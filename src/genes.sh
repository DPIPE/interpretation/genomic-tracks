#!/usr/bin/env bash 
# latest RefSeq curated
mkdir -p data/genes
cd data/genes
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/ncbiRefSeqCurated.txt.gz
gunzip ncbiRefSeqCurated.txt.gz 

# format expected:
# head -2 ncbiRefSeqCurated.txt
# 585     NR_026818.1     chr1    -       34610   36081   36081   36081   3       34610,35276,35720,      35174,35481,36081,      0       FAM138A none    none       -1,-1,-1,
# 585     NM_001005484.2  chr1    +       65418   71585   65564   70008   3       65418,65519,69036,      65433,65573,71585,      0       OR4F5   cmpl    cmpl       -1,0,0,

DATE=$(date +"%Y%m%d")

echo "Create NCBI gene track dated with ${DATE}"

cut -f 2-  ncbiRefSeqCurated.txt |\
genePredToGtf file stdin -utr source=ncbiRefSeqCurated_${DATE} ncbiRefSeqCurated.gtf
sort  -t $'\t' -k1,1V -k4,4n -k5,5n ncbiRefSeqCurated.gtf > ncbiRefSeqCurated.sorted.gtf
bgzip -c ncbiRefSeqCurated.sorted.gtf > ncbiRefSeqCurated.sorted.gtf.gz
tabix -p gff ncbiRefSeqCurated.sorted.gtf.gz
cp ncbiRefSeqCurated.sorted.gtf.gz* ${MOUNT_DIR}

##### 
# make a backbone RefSeq cover set to represent non-redundant gene loci
mkdir -p temp
f=ncbiRefSeqCurated.txt
pf=${f%.*}
# single-isoform loci
gawk 'NR==FNR{
    a[$1];
    next
} $13 in a' <(
    gawk '{
        a[$13]=a[$13]+1;
        next
    } END{
        for (i in a) print i, a[i]
    }' $f | gawk '$2==1'
) $f \
    | gawk '!($3~"_")' > temp/${pf}.single.txt

# multiple-isoform loci
gawk 'NR==FNR{
    a[$1];
    next
} $13 in a' <(
    gawk '{
        a[$13]=a[$13]+1;
        next
    } END{
        for (i in a) {
            ii=i;
            gsub("/","_",ii);
            print ii, a[i]
        }
    }' $f | gawk '$2>1'
) $f \
    | gawk '!($3~"_")' > temp/${pf}.multi.txt

# collapse the multi-isoform loci
gawk -v OFS="\t" '{
    id=$13":"$3":"$4; 
    a[id]=(!(id in a) || $5<a[id])?$5:a[id]; 
    b[id]=(!(id in b) || $6>b[id])?$6:b[id];
    co[id]++;
    next
} END{
    for (i in a) {
        split(i,c,":"); 
        print c[2],a[i],b[i], c[1], co[i], c[3]
    }
}' temp/ncbiRefSeqCurated.multi.txt \
    | sort -k1,1V -k2,2n > temp/ncbiRefSeqCurated.multi.cover.bed
# pool back single and collapse multi-isoform loci
gawk -v OFS="\t" '{
    print $3, $5, $6, $13, 0, $4
}' temp/ncbiRefSeqCurated.single.txt > ncbiRefSeqCurated.cover.bed
cat temp/ncbiRefSeqCurated.multi.cover.bed >> ncbiRefSeqCurated.cover.bed
# sort by coordinate
sort -k1,1V -k2,2n ncbiRefSeqCurated.cover.bed > ncbiRefSeqCurated.cover.bed1
mv ncbiRefSeqCurated.cover.bed1 ncbiRefSeqCurated.cover.bed
# extract non-redundant list of loci
cut -f4 ncbiRefSeqCurated.cover.bed > ncbiRefSeqCurated.cover.symbols.txt

cd .. 
