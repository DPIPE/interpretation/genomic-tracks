#!/usr/bin/env bash 
mkdir -p data/dgv/
cd data/dgv/
## this does not display in igv.js 
wget https://hgdownload.soe.ucsc.edu/gbdb/hg19/dgv/dgvGold.bb 
mv dgvGold.bb ${MOUNT_DIR}

cd ../..