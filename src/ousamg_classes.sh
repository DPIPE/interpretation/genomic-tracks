#!/usr/bin/env bash

# OUSAMG classes files were already supplied as bed
# minimal transformations: pool, keeping the sub-class
# done in TSD due to the sensitive nature of the data

# Pool classes 5/5R/5S and 4 retaining the original class tag
gawk -v OFS="\t" '{
    gsub(".*_","",$4);
    rgb=(($4~"Loss")|| ($4~"Deletion"))?"255,0,0":"0,0,255";
    $4="Class:5;"$4;
    print $1,$2,$3,$4,0,".",$2,$3,rgb
    }' orig_data/Class5_* > tracks/ousamg.class4-5_hg19.pooled.bed
gawk -v OFS="\t" '{
    gsub(".*_","",$4);
    rgb=(($4~"Loss")|| ($4~"Deletion"))?"255,0,0":"0,0,255";
    $4="Class:5R;"$4;
    print $1,$2,$3,$4,0,".",$2,$3,rgb
    }'  orig_data/Class5R* >> tracks/ousamg.class4-5_hg19.pooled.bed
gawk -v OFS="\t" '{
    gsub(".*_","",$4);
    rgb=(($4~"Loss")|| ($4~"Deletion"))?"255,0,0":"0,0,255";
    $4="Class:5S;"$4;
    print $1,$2,$3,$4,0,".",$2,$3,rgb
    }'  orig_data/Class5S* >> tracks/ousamg.class4-5_hg19.pooled.bed
gawk -v OFS="\t" '{
    gsub(".*_","",$4);
    rgb=(($4~"Loss")|| ($4~"Deletion"))?"255,0,0":"0,0,255";
    $4="Class:4;"$4;
    print $1,$2,$3,$4,0,".",$2,$3,rgb
    }'  orig_data/Class4_hg19.bed >> tracks/ousamg.class4-5_hg19.pooled.bed

## sort
sort -k1,1V -k2,2n tracks/ousamg.class4-5_hg19.pooled.bed > tracks/ousamg.class4-5_hg19.pooled.bed1
mv tracks/ousamg.class4-5_hg19.pooled.bed1 tracks/ousamg.class4-5_hg19.pooled.bed

# Format class 1
gawk -v OFS="\t" '{
    rgb=(($4~"Loss")|| ($4~"Deletion"))?"255,0,0":"0,0,255";
    $4="Class:1;"$4;
    print $1,$2,$3,$4,0,".",$2,$3,rgb
    }' orig_data/Class1_CG* | sort -k1,1V -k2,2n > tracks/ousamg.class1_CG_aug2020_hg19.bed


