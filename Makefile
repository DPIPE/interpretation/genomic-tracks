SHELL = /bin/bash

export DOCKER_BUILDKIT=0
export COMPOSE_DOCKER_CLI_BUILD=0
#----
# Set up
#----
MOUNT_DIR = /work/test

#----
# Build image
#----
build:
	docker build . -t genomic-tracks

#----
# Run image
#----
shell:
	docker run -v $(PWD)/../:$(MOUNT_DIR) -it genomic-tracks /bin/bash

#----
# Make tracks
#----
make-tracks: build
	mkdir -p output
	docker run -e MOUNT_DIR=$(MOUNT_DIR) -v $(PWD)/output/:$(MOUNT_DIR) -u $(UID):$(shell id -g) genomic-tracks bash src/run.sh