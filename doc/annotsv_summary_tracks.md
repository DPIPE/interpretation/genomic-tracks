# Documentation of AnnotSV summary tracks

- Assumes: Header set to each given source file - Will break if the file changes in an update. Not the order but the header string match –exit with error
- bed file: selected records are pooled into one file, formatted as bed
- bigwig: same recipe as bed to bigwig implemented in sv-tools (bed_tobigwig.sh) 
- Common filtering: size> SVminSize (default 50bp)
- Types: DEL, DUP, INS, INV

## AnnotSV “Benign” tracks (creation or update)

- Location (relative): SVincludedInFt/BenignSV – SV included in the Feature (default 100%)

List of databases used in the generation of the track:

- gnomAD
    - Source: gnomad_v2.1_sv.sites.bed.gz 
    - Skip the MCNV (multi-allelic CNV)
    - At least one population allele frequency > 1% OR at least 5 homozygous individuals
- DGV
    - Source: GRCh3*_hg*_variants_*.txt
    - ≥ 500 (minTotalNumber) individuals tested
    - allele frequency > 1%
- 1000g
    - Source:ALL.wgs.mergedSV*.vcf.gz
    - Handle multi-allelic sites: split into multiple rows first
    -  allele frequency > 1%
    - Alu, LINE1, SVA written into Gains
- ClinGen HITS
    - Source: ClinGen_gene_curation_list_$genomeBuild.tsv and ClinGen_region_curation_list_$genomeBuild.tsv
    - Haploinsufficiency score equals 40 – add to Losses
    - Triplosensitivity score equals 40 – add to Gains
- ClinVar
    - Source: clinvar*.vcf.gz
    - “benign” or “benign/likely benign” clinical significance (CLNSIG)
    - “criteria_provided”, “_multiple_submitters” or “reviewed_by_expert_panel” SV review status (CLNREVSTAT)
- IMH
    - Source: *callset.public.bedpe.gz
    - Allele frequency > 0.01

## AnnotSV “Pathogenic” tracks (creation or update)

Location(relative): FtIncludedInSV/PathogenicSV – Feature included in SV at 100% (default)

List of databases used in the generation of the track:

- ClinVar
    - Source: clinvar*.vcf.gz
    - “pathogenic” or “pathogenic/likely pathogenic” clinical significance (CLNSIG)
    - “criteria_provided”, “_multiple_submitters” or “reviewed_by_expert_panel” SV review status (CLNREVSTAT)
- ClinGenHITS
    - Source: ClinGen_gene_curation_list_$genomeBuild.tsv and ClinGen_region_curation_list_$genomeBuild.tsv
    - Haploinsufficiency score equals 3 – add to Losses
    - Triplosensitivity score equals 3 – add to Gains
    - Annotates with OMIM loss (for HI==3) and OMIM gain (TS==3) ids and phenotypes
- dbVar
    - Source: genomeBuild.nr_deletions.pathogenic.tsv.gz, genomeBuild.nr_duplications.pathogenic.tsv.gz and genomeBuild.nr_insertions.pathogenic.tsv.gz
    - Types: DEL, DUP, INS
- OMIM
    - Source: *_morbid.tsv.gz
    - Adding all the morbid genes as intervals
